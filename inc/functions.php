<?php
// fonction nav_item en param le lien, le nom de la page et sa classe
function nav_item(string $lien, string $titre, string $linkClasse = ''): string
{
    $classe = 'nav-link';
    //permet de mettre en actif la page courrante
    //si la page est égal au lien on passe en active sinon rien
    if ($_SERVER['SCRIPT_NAME'] === $lien) {
        $classe .= ' active';
    }
    return <<<HTML
    <li class="$classe">
        <a class="$linkClasse" href="$lien">$titre</a>
    </li>
HTML;
}

//affiche le menu
function nav_menu(string $linkClass = ''): string
{
    return
        nav_item('index.php', 'Accueil', $linkClass) .
        nav_item('sport.php', 'Sport', $linkClass);
}
