<?php $title = "Article 2"; ?>
<h1><?php printf($title); ?></h1>
<div class="media">
    <img src="img/img_02.jpg" class="align-self-start mr-3">
    <div class="media-body">
        <h5 class="mt-0">Match</h5>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam exercitationem nostrum, iusto cumque, accusantium dignissimos blanditiis quas in quia molestiae voluptatem nihil vel optio temporibus corporis nesciunt repellat. Nobis, quas.
        </p>
        <p class="mb-0">Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
    </div>
</div>